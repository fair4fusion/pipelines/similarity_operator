import json
import numpy as np
import os
import glob
import sys
from heapq import nsmallest
from operator import indexOf
import yaml

# fn = sys.argv[1]
file = "config.yml"
with open(file,"r") as file :
    paths = yaml.safe_load(file)

path_in = paths['paths']['input']
number_of_sim = int(paths['number'])
distances = []
idd = []

for filename in glob.glob(os.path.join(path_in, '*.distance')):
    a = filename[len(path_in):].lstrip("/")
    (file, ext) = os.path.splitext(a)
    # print("Filename without extension =", file)
    idd.append(int(file))
    with open(os.path.join(os.getcwd(), filename), 'r') as f:
        value = np.fromfile(f, dtype=float, count=-1, sep=' ')
        distances.append(value)

a = nsmallest(number_of_sim, distances)
# print(a)

indices = []

for i in a:
    if i in distances:
        indices.append(distances.index(i))

similar_values = []

for i in indices:
    similar_values.append(idd[i])

# print(similar_values)