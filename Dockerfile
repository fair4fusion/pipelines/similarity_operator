FROM python:3.9-slim

RUN apt update && apt install -y git \
	gcc \
	make

RUN apt-get install python3-pip -y

COPY requirements.txt /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt

RUN mkdir data/

COPY similarity.py similarity.py
# COPY /out /out
# EXPOSE 8000
ENTRYPOINT ["python3","similarity.py"]
